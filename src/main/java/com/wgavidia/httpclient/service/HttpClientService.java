package com.wgavidia.httpclient.service;

import com.wgavidia.httpclient.model.Pet;
import com.wgavidia.httpclient.restclients.HttpClientRestClient;
import com.wgavidia.httpclient.restclients.RestClient;
import com.wgavidia.httpclient.util.RestConstants;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Class to call the rest client {@linkplain HttpClientRestClient} and execute the methods to obtain the required information.
 * <br /><br />
 * <b>Note:</b> this class is only a bridge between the rest client and the API class but could be used to implement business logic.
 *
 * @author wgavidia
 */
@Service
public class HttpClientService {

    private final RestClient restClient;

    public HttpClientService(HttpClientRestClient restClient) {
        this.restClient = restClient;
    }

    public List<Pet> getPetsByStatus() throws Exception {
        return restClient.getPetsByStatus(RestConstants.PET_WEB_SERVICE_PARAMETER_VALUE);
    }

}
