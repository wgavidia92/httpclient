package com.wgavidia.httpclient.restclients;

import com.wgavidia.httpclient.model.Pet;
import com.wgavidia.httpclient.util.RestConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Sealed interface to define the method {@linkplain #getPetsByStatus(String)} which will be implemented by the concrete classes:
 * <ul>
 *     <li>{@linkplain HttpUrlConnectionClient}</li>
 *     <li>{@linkplain HttpClientRestClient}</li>
 *     <li>{@linkplain RestTemplateClient}</li>
 *     <li>{@linkplain WebClientRestClient}</li>
 * </ul>
 *
 * This interface also define some default methods used by those classes.
 *
 * @author wgavidia
 */
public sealed interface RestClient permits HttpUrlConnectionClient, HttpClientRestClient, RestTemplateClient, WebClientRestClient {

    Gson gson = new Gson();

    /**
     * Method to perform the GET call and return an List of objects. See the implementations at:
     *
     * <ul>
     *     <li>{@linkplain HttpUrlConnectionClient#getPetsByStatus(String)}</li>
     *     <li>{@linkplain HttpClientRestClient#getPetsByStatus(String)}</li>
     *     <li>{@linkplain RestTemplateClient#getPetsByStatus(String)}</li>
     *     <li>{@linkplain WebClientRestClient#getPetsByStatus(String)}</li>
     * </ul>
     *
     * @param status {@linkplain  String} parameter to add a filter in the request
     * @return {@linkplain List}&lt;{@linkplain Pet}&gt;
     * @throws Exception
     */
    List<Pet> getPetsByStatus(String status) throws Exception;

    /**
     * Creates a {@linkplain Type} to map from Json string to {@linkplain List}&lt;{@linkplain Pet}&gt;
     * @return {@linkplain Type} of {@linkplain List}&lt;{@linkplain Pet}&gt;
     */
    default Type getPetListType() {
        return new TypeToken<List<Pet>>(){}.getType();
    }

    /**
     * Creates a URI object with the final endpoint using the parameter received.
     *
     * @param status {@linkplain String} status to query the pets
     * @return {@linkplain URI} Object with the host and query parameters
     * @throws Exception
     */
    default URI generateEndPoint(String status) throws Exception {
        return UriComponentsBuilder.newInstance()
                .uri(new URI(RestConstants.PET_WEB_SERVICE))
                .queryParam(RestConstants.PET_WEB_SERVICE_PARAMETER, status)
                .build()
                .toUri();
    }

    /**
     * Validates if the statusCode from the parameter is different of {@linkplain HttpStatus#OK},
     * if so, the status code is logged and an Exception is thrown with a generic message
     *
     * @param statusCode {@code int} code from the response
     * @param log {@linkplain Logger} object to log the messages
     * @throws Exception
     */
    default void validateResponseStatus(int statusCode, Logger log) throws Exception {
        if (statusCode != HttpStatus.OK.value()) {
            log.log(Level.SEVERE, "Error getting the response", statusCode);
            throw new Exception("Bad respose from web service");
        }
    }

    /**
     * Method to map a Json String to a {@linkplain List}&lt;{@linkplain Pet}&gt; using {@linkplain Gson}
     * @param json {@linkplain String} json to map
     * @return {@linkplain List}&lt;{@linkplain Pet}&gt; got from the transformation
     */
    default List<Pet> getPetsFromJson(String json) {
        return gson.fromJson(json, getPetListType());
    }

}
