package com.wgavidia.httpclient.restclients;

import com.wgavidia.httpclient.model.Pet;
import com.wgavidia.httpclient.util.RestConstants;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a rest client implementing {@linkplain HttpURLConnection} to perform the web service calls.
 * <br/><br/>
 * <b>Note:</b> {@linkplain HttpURLConnection} class was defined since java 1.1 and is not the best way to perform a rest call.
 * Look at the other rest clients defined in this package.
 *
 * @see HttpClientRestClient
 * @see RestTemplateClient
 * @see WebClientRestClient
 *
 * @author wgavidia
 */
@Component
public final class HttpUrlConnectionClient implements RestClient {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public List<Pet> getPetsByStatus(String status) throws Exception {
        try {
            // Creates the objects required to perform the call.
            var url = generateEndPoint(status).toURL();
            log.log(Level.INFO, RestConstants.ENDPOINT_WEB_SERVICE_LOG, new Object[]{url, this.getClass().getName()});
            var httpConnection = (HttpURLConnection) url.openConnection();

            // Sets the required properties, ie: headers, etc.
            httpConnection.setRequestMethod(HttpMethod.GET.name());
            httpConnection.setRequestProperty("Accept", MediaType.APPLICATION_JSON_VALUE);

            // Validates the response code to proceed or throw an exception.
            validateResponseStatus(httpConnection.getResponseCode(), log);

            // Variables to read and store the response.
            var jsonResponse = new StringBuilder();
            String responseLine;

            // Gets a BufferedReader from the response.
            var reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));

            // Reads the response from the BufferedReader object
            while ((responseLine = reader.readLine()) != null)
                jsonResponse.append(responseLine);

            // Process the jsonString to the List of objects using Gson
            return getPetsFromJson(jsonResponse.toString());
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new Exception("Error getting the response from the web service");
        }
    }

}
