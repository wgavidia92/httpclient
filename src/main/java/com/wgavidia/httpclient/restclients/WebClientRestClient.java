package com.wgavidia.httpclient.restclients;

import com.wgavidia.httpclient.model.Pet;
import com.wgavidia.httpclient.util.RestConstants;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a rest client using {@linkplain WebClient} to perform the web service calls.
 * <br/><br/>
 * <b>Note:</b> {@linkplain WebClient} interface is a definition to make synchronous and asynchronous calls to web services.
 * This interface is present since Spring 5.0 and use Reactor Netty by default.
 *
 * @see HttpUrlConnectionClient
 * @see HttpClientRestClient
 * @see RestTemplateClient
 *
 * @author wgavidia
 */
@Component
public final class WebClientRestClient implements RestClient {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public List<Pet> getPetsByStatus(String status) throws Exception {
        try {
            var endpoint = generateEndPoint(status).toString();
            log.log(Level.INFO, RestConstants.ENDPOINT_WEB_SERVICE_LOG, new String[]{endpoint, this.getClass().getName()});
            // Creates the object required to perform the call.
            var webClient = WebClient.builder()
                    .baseUrl(endpoint)
                    .build();

            // Gets the response using restTemplate injected and defined in the method HttpClientApplication@restTemplate
            var response = webClient.get()
                    .retrieve()
                    .onStatus(HttpStatus::isError, clientResponse -> {
                        log.log(Level.SEVERE, "Error getting the response", clientResponse.statusCode());
                        throw new RuntimeException("Bad respose from web service");
                    })
                    .bodyToMono(String.class)
                    .block();

            return getPetsFromJson(response);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new Exception("Error getting the response from the web service");
        }
    }

}
