package com.wgavidia.httpclient.restclients;

import com.wgavidia.httpclient.model.Pet;
import com.wgavidia.httpclient.util.RestConstants;

import org.springframework.stereotype.Component;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a rest client implementing {@linkplain HttpClient} to perform the web service calls.
 * <br/><br/>
 * <b>Note:</b> {@linkplain HttpClient} class was defined since java 11, make sure you are using a valid version to run it.
 * Look at the other rest clients defined in this package.
 *
 * @see HttpUrlConnectionClient
 * @see RestTemplateClient
 * @see WebClientRestClient
 *
 * @author wgavidia
 */
@Component
public final class HttpClientRestClient implements RestClient {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public List<Pet> getPetsByStatus(String status) throws Exception {
        try {
            // Creates the objects required to perform the call.
        	var endpoint = generateEndPoint(status);
        	log.log(Level.INFO, RestConstants.ENDPOINT_WEB_SERVICE_LOG, new Object[]{endpoint, this.getClass().getName()});
            var httpClient = HttpClient.newBuilder().build();
            var httpRequest = HttpRequest.newBuilder(endpoint)
                    .GET()
                    .build();

            // Gets the response with the HttpClient object and the request
            var response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

            // Validates the response code to proceed or throw an exception.
            validateResponseStatus(response.statusCode(), log);

            return getPetsFromJson(response.body());
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new Exception("Error getting the response from the web service");
        }
    }

}
