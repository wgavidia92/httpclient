package com.wgavidia.httpclient.restclients;

import com.wgavidia.httpclient.model.Pet;
import com.wgavidia.httpclient.util.RestConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a rest client implementing {@linkplain RestTemplate} to perform the web service calls.
 * <br/><br/>
 * <b>Note:</b> {@linkplain RestTemplate} class is an implementation to make synchronized calls to web services.
 *
 * @see HttpUrlConnectionClient
 * @see HttpClientRestClient
 * @see WebClientRestClient
 *
 * @author wgavidia
 */
@Component
public final class RestTemplateClient implements RestClient {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Pet> getPetsByStatus(String status) throws Exception {
        try {
            // Gets the response using restTemplate injected and defined in the method HttpClientApplication@restTemplate
        	var endpoint =  generateEndPoint(status);
        	log.log(Level.INFO, RestConstants.ENDPOINT_WEB_SERVICE_LOG, new Object[]{endpoint, this.getClass().getName()});
        	var response =
                    restTemplate.getForEntity(endpoint, String.class);

            // Validates the response code to proceed or throw an exception.
            validateResponseStatus(response.getStatusCodeValue(), log);

            return getPetsFromJson(response.getBody());
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new Exception("Error getting the response from the web service");
        }
    }
}
