package com.wgavidia.httpclient.api;

import com.wgavidia.httpclient.service.HttpClientService;
import com.wgavidia.httpclient.service.HttpUrlConnectionService;
import com.wgavidia.httpclient.service.RestTemplateService;
import com.wgavidia.httpclient.service.WebClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pets")
public class PetApi {

    @Autowired
    private HttpUrlConnectionService httpUrlConnectionService;

    @Autowired
    private HttpClientService httpClientService;

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private WebClientService webClientService;

    @GetMapping(value = "/http-url-connection", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getPetsByStatusWithHttpUrlConnectionClass() {
        try {
            return ResponseEntity.ok(httpUrlConnectionService.getPetsByStatus());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping(value = "/http-client", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getPetsByStatusWithHttpClientClass() {
        try {
            return ResponseEntity.ok(httpClientService.getPetsByStatus());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping(value = "/rest-template", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getPetsByStatusWithRestTemplateClass() {
        try {
            return ResponseEntity.ok(restTemplateService.getPetsByStatus());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping(value = "/web-client", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getPetsByStatusWithWebClientInterface() {
        try {
            return ResponseEntity.ok(webClientService.getPetsByStatus());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

}
