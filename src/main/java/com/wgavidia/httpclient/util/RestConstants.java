package com.wgavidia.httpclient.util;

public final class RestConstants {

    public static final String PET_WEB_SERVICE = "https://petstore.swagger.io/v2/pet/findByStatus";
    public static final String PET_WEB_SERVICE_PARAMETER = "status";
    public static final String PET_WEB_SERVICE_PARAMETER_VALUE = "sold";

    public static final String ENDPOINT_WEB_SERVICE_LOG = "Calling endpoint: {0} from {1}";

}
