package com.wgavidia.httpclient.model;

public class Pet {

    private long id;
    private String status;
    private Category category;

    public long getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public Category getCategory() {
        return category;
    }

}
