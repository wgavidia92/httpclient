package com.wgavidia.httpclient.model;

public class Category {

    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
