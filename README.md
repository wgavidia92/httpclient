# httpclient

Project to show the use of different http java clients, _you can find all the code in the **develop** branch._

### APIs

* [HttpURLConnection - since JDK1.1](https://docs.oracle.com/javase/8/docs/api/java/net/HttpURLConnection.html)
* [HttpClient - since JDK11](https://docs.oracle.com/en/java/javase/17/docs/api/java.net.http/java/net/http/HttpClient.html)
* [RestTemplate - since Spring 3.0](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/client/RestTemplate.html)
* [WebClient - since Spring 5.0](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/reactive/function/client/WebClient.html)

>
>This is a basic example with synchronous calls only. If you need further information related to asynchronous calls, please take a look at the documentation of [HttpClient - since JDK11](https://docs.oracle.com/en/java/javase/17/docs/api/java.net.http/java/net/http/HttpClient.html) and [WebClient - since Spring 5.0](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/reactive/function/client/WebClient.html)
>
## Getting started

For this project you should have configured the following tools and versions:

* JDK17
* Maven 3.8.3 or above

You should have knowledge working with java 17 and Spring. Also, be aware if the following API is up due to this is used in the ws calls:

> API: https://petstore.swagger.io/
>
> Endpoint: https://petstore.swagger.io/pet/findByStatus?status=sold

## Architecture

This is a monolitic application and you can see the sequence diagrams to get a better understantind of the components below

![sequence1](https://gitlab.com/wgavidia92/httpclient/-/raw/main/diagrams/first_endpoint.png "Sequence diagram")
![sequence1](https://gitlab.com/wgavidia92/httpclient/-/raw/main/diagrams/second_endpoint.png "Sequence diagram")
![sequence1](https://gitlab.com/wgavidia92/httpclient/-/raw/main/diagrams/third_endpoint.png "Sequence diagram")
![sequence1](https://gitlab.com/wgavidia92/httpclient/-/raw/main/diagrams/forth_endpoint.png "Sequence diagram")

## Calling the endpoints

You can use either a rest client or your browser to test the following endpoints:

* http://localhost:8080/pets/http-url-connection
* http://localhost:8080/pets/http-client
* http://localhost:8080/pets/rest-template
* http://localhost:8080/pets/web-client

> Keep in mind the above URLs are the default ones for Spring. If you change the application to use a different port you should change the port in the URLs as well.
